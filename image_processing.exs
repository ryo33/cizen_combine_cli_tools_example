defmodule UpdateCPUUsage do
  defstruct [:value]
end

defmodule Done do
  defstruct [:image]
end

defmodule ImageProcessor do
  use Cizen.Automaton
  defstruct [:image]

  alias Cizen.Effects.Dispatch

  @impl true
  def spawn(_id, %__MODULE__{image: image}) do
    image
  end

  @impl true
  def yield(id, image) do
    IO.puts("start processing: #{image}")
    {_, 0} = System.cmd("bash", ["process.sh", image])
    perform id, %Dispatch{
      body: %Done{image: image}
    }
    IO.puts("done: #{image}")
    Cizen.Automaton.finish() # Finish itself
  end
end

defmodule CPUMonitor do
  use Cizen.Automaton
  defstruct []

  alias Cizen.Effects.Dispatch

  @impl true
  def spawn(_id, %__MODULE__{}) do
    alias Porcelain.Process, as: Proc
    %Proc{out: outstream} = Porcelain.spawn("bash", ["monitor-cpu.sh"], [out: :stream])
    outstream
  end

  @impl true
  def yield(id, outstream) do
    Enum.each(outstream, fn line ->
      value =
        line
        |> String.trim()
        |> String.to_integer()
      IO.puts("cpu usage updated: #{value}")
      perform id, %Dispatch{
        body: %UpdateCPUUsage{
          value: value
        }
      }
    end)
    Cizen.Automaton.finish() # Finish itself
  end
end

defmodule Scheduler do
  use Cizen.Automaton
  defstruct [:images]

  alias Cizen.{Event, Filter}
  alias Cizen.Effects.{Start, Subscribe, Receive}

  @impl true
  def spawn(id, %__MODULE__{images: images}) do
    perform id, %Subscribe{
      event_filter: Filter.any([
        Filter.new(fn %Event{body: %UpdateCPUUsage{}} -> true end),
        Filter.new(fn %Event{body: %Done{}} -> true end)
      ])
    }
    %{
      images: images,
      lent_resources: 0,
      max_resources: 4
    }
  end

  @impl true
  def yield(_id, %{images: []}), do: Cizen.Automaton.finish() # Finish itself
  def yield(id, %{images: [image | tail]} = state) do
    state = %{state | images: tail}
    state = borrow_resource(id, state)
    perform id, %Start{
      saga: %ImageProcessor{image: image}
    }
    state
  end

  defp borrow_resource(id, state) do
    %{
      lent_resources: lent_resources,
      max_resources: max_resources
    } = state
    if lent_resources < max_resources do
      %{state | lent_resources: lent_resources + 1}
    else
      event = perform id, %Receive{
        event_filter: Filter.any([
          Filter.new(fn %Event{body: %UpdateCPUUsage{}} -> true end),
          Filter.new(fn %Event{body: %Done{}} -> true end)
        ])
      }
      case event.body do
        %UpdateCPUUsage{value: value} when value < 20 ->
          borrow_resource(id, %{state | max_resources: 16})

        %UpdateCPUUsage{value: value} when value < 40 ->
          borrow_resource(id, %{state | max_resources: 8})

        %UpdateCPUUsage{value: value} when value < 60 ->
          borrow_resource(id, %{state | max_resources: 4})

        %UpdateCPUUsage{value: value} when value < 80 ->
          borrow_resource(id, %{state | max_resources: 2})

        %UpdateCPUUsage{value: _value} ->
          borrow_resource(id, %{state | max_resources: 1})

        %Done{} ->
          borrow_resource(id, %{state | lent_resources: lent_resources - 1})
      end
    end
  end
end

defmodule Main do
  use Cizen.Effectful # to use handle/1
  alias Cizen.{Event, Filter}
  alias Cizen.Effects.{Start, Subscribe, Receive}

  def main do
    images = Enum.map(1..50, &("image#{&1}.png"))
    handle fn id ->
      perform id, %Subscribe{
        event_filter: Filter.new(fn %Event{body: %Done{}} -> true end)
      }
      perform id, %Start{
        saga: %CPUMonitor{}
      }
      perform id, %Start{
        saga: %Scheduler{images: images}
      }
      # Wait until all images are processed
      for _ <- images do
        perform id, %Receive{}
      end
    end
  end
end

Main.main()

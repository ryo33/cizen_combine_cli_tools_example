#!/bin/bash

echo start processing $1
# sleeps 1-10 seconds
sleep $(echo $RANDOM % 10 + 1 | bc)
echo finish processing $1

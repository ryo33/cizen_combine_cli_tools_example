# CizenCollaborateTools

## Run

```bash
mix deps.get
mix run image_processing.exs
```

## Update CPU usage

```bash
echo 100 >> cpu-logs
echo 5 >> cpu-logs
```

defmodule CizenCollaborateTools do
  @moduledoc """
  Documentation for CizenCollaborateTools.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CizenCollaborateTools.hello()
      :world

  """
  def hello do
    :world
  end
end

defmodule CizenCollaborateTools.MixProject do
  use Mix.Project

  def project do
    [
      app: :cizen_collaborate_tools,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cizen, "~> 0.12.5"},
      {:porcelain, "~> 2.0.0"}
    ]
  end
end
